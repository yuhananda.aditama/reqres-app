package id.bootcamp.reqres.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentListUserBinding
import id.bootcamp.reqres.ui.adapter.UserListAdapter
import id.bootcamp.reqres.ui.data.User
import id.bootcamp.reqres.ui.viewmodel.ListUserViewModel
import id.bootcamp.reqres.ui.viewmodel.ReqresViewModelFactory

class ListUserFragment : Fragment(), MenuProvider {
    private lateinit var binding: FragmentListUserBinding
    private lateinit var viewModel: ListUserViewModel

    val onDeleteClickListener: DeleteUserFragment.OnDeleteClickListener =
        object : DeleteUserFragment.OnDeleteClickListener {
            override fun onDeleteClick(user: User) {
                viewModel.deleteUser(user.id)
            }

        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentListUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, ReqresViewModelFactory.getInstance(requireContext()))
            .get(ListUserViewModel::class.java)

        //Set Menu
        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(binding.toolbar)
        activity.supportActionBar?.title = "List user"

        //Set Fragment Toolbar Menu
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)

        //Observer
        viewModel.listUserLiveData.observe(viewLifecycleOwner) { listData ->
            if (listData != null) {
                val adapter = UserListAdapter(listData)
                binding.rvListUser.adapter = adapter
                binding.rvListUser.layoutManager = LinearLayoutManager(requireContext())
                viewModel.listUserLiveData.postValue(null)

                adapter.setOnUserItemClickCallback(object :
                    UserListAdapter.OnUserItemClickListener {
                    override fun onItemClick(user: User) {
                        val bundle = Bundle()
                        bundle.putParcelable(DetailUserFragment.EXTRA_USER, user)
                        //Navigasi ke detail user fragment
                        binding.rvListUser.findNavController()
                            .navigate(R.id.action_listUserFragment_to_detailUserFragment, bundle)
                    }

                    override fun onEditClick(user: User) {
                        val bundle = Bundle()
                        bundle.putParcelable(EditUserFragment.EXTRA_USER, user)

                        binding.root.findNavController()
                            .navigate(R.id.action_listUserFragment_to_editUserFragment, bundle)
                    }

                    override fun onDeleteClick(user: User) {
                        val deleteUserFragment = DeleteUserFragment()

                        val bundle = Bundle()
                        bundle.putParcelable(DeleteUserFragment.EXTRA_USER, user)

                        deleteUserFragment.arguments = bundle
                        val layoutManager = childFragmentManager
                        deleteUserFragment.show(
                            layoutManager,
                            DeleteUserFragment::class.java.simpleName
                        )
                    }

                })
            }
        }
        viewModel.getListUser()

        viewModel.isDeleteSuccess.observe(viewLifecycleOwner){
            if (it != null){
                val pesan = if (it){
                    "Berhasil delete"
                } else{
                    "Gagal delete"
                }
                Toast.makeText(requireContext(), pesan, Toast.LENGTH_SHORT).show()
                viewModel.isDeleteSuccess.postValue(null)
            }
        }
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_list_user, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        //jika menu id itu R.id.menuAddUser maka .....
        when (menuItem.itemId) {
            R.id.menuAddUser -> {
                binding.toolbar.findNavController()
                    .navigate(R.id.action_listUserFragment_to_addUserFragment)
            }
        }
        return false
    }

}