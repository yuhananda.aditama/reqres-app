package id.bootcamp.reqres.ui

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentDeleteUserBinding
import id.bootcamp.reqres.ui.data.User

class DeleteUserFragment : DialogFragment() {
    private lateinit var binding: FragmentDeleteUserBinding
    private var onDeleteClickListener: OnDeleteClickListener? = null
    private var user: User? = null

    interface OnDeleteClickListener {
        fun onDeleteClick(user: User)
    }

    companion object {
        const val EXTRA_USER = "EXTRA_USER"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentDeleteUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                user = arguments?.getParcelable(EXTRA_USER, User::class.java)
            } else {
                user = arguments?.getParcelable(EXTRA_USER)
            }
        }

        binding.tvDeleteMsg.text = "Apakah anda yakin mau delete ${user?.firstName} ${user?.lastName}?"

        binding.btnNo.setOnClickListener {
            dialog?.dismiss()
        }

        binding.btnYes.setOnClickListener {
            onDeleteClickListener?.onDeleteClick(user as User)
            dialog?.dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val fragment = parentFragment
        if (fragment is ListUserFragment) {
            //Pasang listener/ Setup Listener
            this.onDeleteClickListener = fragment.onDeleteClickListener
        }
    }

    override fun onDetach() {
        super.onDetach()
        //Bersih-bersih
        onDeleteClickListener = null
    }

}