package id.bootcamp.reqres.data

import id.bootcamp.reqres.data.local.room.ExampleDao
import id.bootcamp.reqres.data.remote.response.DataItem
import id.bootcamp.reqres.data.remote.retrofit.RegresApiService

class ExampleRepository private constructor(
    private val regresApiService: RegresApiService,
    private val exampleDao: ExampleDao
) {
    companion object {
        @Volatile
        private var instance: ExampleRepository? = null
        fun getInstance(
            regresApiService: RegresApiService,
            exampleDao: ExampleDao
        ): ExampleRepository =
            instance ?: synchronized(this) {
                instance ?: ExampleRepository(regresApiService, exampleDao)
            }.also { instance = it }
    }

    suspend fun getExample(): ExampleViewData {
        //Get From API
        var firstDataApi = DataItem()
        try {
            val responseApi = regresApiService.getUser(1)
            firstDataApi = responseApi.data?.get(0) ?: DataItem()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //Get From LocalDB
        val responseDb = exampleDao.getExamples()
        val firstDataDb = responseDb[0]

        //Convert Data untuk View
        val exampleViewData = ExampleViewData(
            firstDataDb.title,
            firstDataDb.desc,
            firstDataApi.firstName ?: "",
            firstDataApi.lastName ?: ""
        )
        return exampleViewData
    }
}