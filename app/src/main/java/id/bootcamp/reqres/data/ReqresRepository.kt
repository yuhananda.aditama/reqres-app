package id.bootcamp.reqres.data

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.bootcamp.reqres.data.remote.request.ReqresCreateRequest
import id.bootcamp.reqres.data.remote.request.ReqresEditRequest
import id.bootcamp.reqres.data.remote.request.ReqresLoginRequest
import id.bootcamp.reqres.data.remote.request.ReqresRegisterRequest
import id.bootcamp.reqres.data.remote.response.ReqresListUserResponse
import id.bootcamp.reqres.data.remote.response.ReqresLoginResponse
import id.bootcamp.reqres.data.remote.response.ReqresRegisterResponse
import id.bootcamp.reqres.data.remote.retrofit.RegresApiService
import id.bootcamp.reqres.ui.data.LoginData
import id.bootcamp.reqres.ui.data.RegisterData
import id.bootcamp.reqres.ui.data.User
import org.json.JSONObject
import retrofit2.HttpException


class ReqresRepository(val reqresApiService: RegresApiService) {

    suspend fun login(email: String, password: String): LoginData {
//        return LoginData(false,
//            "Login Gagal: Email atau Password Salah")
        val requestBody = ReqresLoginRequest(email = email, password = password)
        try { //Ketika login success / codenya 200 - 399
            val data = reqresApiService.login(requestBody)
            return LoginData(true, "Login Success!")
        } catch (e: HttpException) {//400 - 599
            e.printStackTrace() //print error log
            if (e.code() == 400) {
                //3 baris dibawah untuk konvert string ke object RegresLoginResponse
                val gson =
                    Gson() //"{"error":"Missing email or username"}" -> object RegresLoginResponse
                val type = object : TypeToken<ReqresLoginResponse>() {}.type
                val errorResponse: ReqresLoginResponse? =
                    gson.fromJson(e.response()?.errorBody()?.charStream(), type)
                return LoginData(false, errorResponse?.error as String)
            } else {
                return LoginData(false, "Terjadi kesalahan!")
            }
        } catch (e: Exception) { //Error lainnya
            e.printStackTrace() //print error log
            return LoginData(false, e.message as String)
        }
    }

    suspend fun register(email: String, password: String): RegisterData {
//        return RegisterData(
//            false,
//            "Register Gagal: Email sudah terdaftar"
//        )
        val requestBody = ReqresRegisterRequest(email = email, password = password)
        try { //Ketika login success / codenya 200 - 399
            val data = reqresApiService.register(requestBody)
            return RegisterData(true, "Register Success!")
        } catch (e: HttpException) {//400 - 599
            e.printStackTrace() //print error log
            if (e.code() == 400) {
                //3 baris dibawah untuk konvert string ke object RegresLoginResponse
                val gson =
                    Gson() //"{"error":"Missing email or username"}" -> object RegresLoginResponse
                val type = object : TypeToken<ReqresRegisterResponse>() {}.type
                val errorResponse: ReqresRegisterResponse? =
                    gson.fromJson(e.response()?.errorBody()?.charStream(), type)
                return RegisterData(false, errorResponse?.error as String)
            } else {
                return RegisterData(false, "Terjadi kesalahan!")
            }
        } catch (e: Exception) { //Error lainnya
            e.printStackTrace() //print error log
            return RegisterData(false, e.message as String)
        }
    }

    //nama fungsi getUserList()
    //Deklarasi atau membuat fungsi getUserList()
    suspend fun getUserList(): List<User> {//: List<User> -> return valuenya tipedatanya adalah List<User>
        try {
            //Perintah memanggil API
            val data: ReqresListUserResponse = reqresApiService.getUser(1)
            //Melakukan mapping/perulangan tiap data di data.data
            //Mengubah tipe data List<DataItem> -> List<User>
            val userList: List<User>? = data.data?.map { dataItem ->
                //Membuat objek user
                User(
                    dataItem?.id ?: 0,
                    dataItem?.avatar ?: "",
                    dataItem?.firstName ?: "",
                    dataItem?.lastName ?: "",
                    dataItem?.email ?: ""
                )
            }
            return userList ?: ArrayList()
        } catch (e: Exception) {
            //Cetak Error
            e.printStackTrace()
            return ArrayList()
        }


//        Fake data
//        val arrUser = ArrayList<User>() //Bikin array kosong bertipe data User
//
//        //arrUser.add() -> Fungsi menambahkan data object User ke dalam variabel arrUser
//        //val user = User(avatar,firstName,lastName,email)
//        //-> Membuat object user berdasarkan class User
//        val user = User(
//            "https://static.vecteezy.com/system/resources/previews/002/002/257/non_2x/beautiful-woman-avatar-character-icon-free-vector.jpg",
//            "John 1", "Doe 1", "john1@gmail.com")
//        arrUser.add(user)
//        arrUser.add(User(
//            "https://static.vecteezy.com/system/resources/previews/002/002/257/non_2x/beautiful-woman-avatar-character-icon-free-vector.jpg",
//            "John 2", "Doe 2", "john2@gmail.com"
//        ))
//        arrUser.add(User(
//            "https://static.vecteezy.com/system/resources/previews/002/002/257/non_2x/beautiful-woman-avatar-character-icon-free-vector.jpg",
//            "John 3", "Doe 3", "john3@gmail.com"
//        ))
//        return arrUser
    }

    suspend fun deleteUser(id: Int): Boolean {
        try {
            val data = reqresApiService.deleteUserById(id)
            return data.code() == 204
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    suspend fun addUser(name: String, job: String): User {
        try {
            val reqresRequest = ReqresCreateRequest(name, job)
            val data = reqresApiService.insertUser(reqresRequest)
            val user = User(data.id?.toInt() ?: 1, "", data.name ?: "", "", "")
            return user
        } catch (e: Exception) {
            e.printStackTrace()
            return User(1, "", "", "", "")
        }
    }

    suspend fun editUser(id: Int, name: String, job: String): User {
        try {
            val reqresRequest = ReqresEditRequest(name, job)
            val data = reqresApiService.editUser(reqresRequest,id)
            val user = User(data.id?.toInt() ?: 1, "", data.name ?: "", "", "")
            return user
        } catch (e: Exception) {
            e.printStackTrace()
            return User(1, "", "", "", "")
        }
    }


    companion object {
        @Volatile
        private var instance: ReqresRepository? = null
        fun getInstance(reqresApiService: RegresApiService): ReqresRepository =
            instance ?: synchronized(this) {
                instance ?: ReqresRepository(reqresApiService)
            }.also { instance = it }
    }
}