package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.ui.data.RegisterData
import kotlinx.coroutines.launch

class RegisterViewModel(val reqresRepository: ReqresRepository) : ViewModel() {

    val registerLiveData = MutableLiveData<RegisterData>()

    fun register(email: String, password: String) = viewModelScope.launch{
        //Aksi ketika register
        val registerData = reqresRepository.register(email,password)
        registerLiveData.postValue(
            registerData
        )
    }
}