package id.bootcamp.reqres.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.bootcamp.reqres.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val factory: ExampleViewModelFactory = ExampleViewModelFactory.getInstance(this)
//        val viewModel: ExampleViewModel by viewModels { factory }
//
//        binding.pgBar.visibility = View.VISIBLE
//        viewModel.getExampleData()
//        viewModel.exampleData.observe(this) { result ->
//            if (result != null) {
//                binding.pgBar.visibility = View.GONE
//                binding.txView.text = "title : ${result.title}" +
//                        "\ndesc : ${result.desc}" +
//                        "\nfirstName : ${result.firstName}" +
//                        "\nlastName : ${result.lastName}"
//            }
//        }
    }
}