package id.bootcamp.reqres.data

class ExampleViewData(
    val title: String,
    val desc: String,
    val firstName: String,
    val lastName: String
)