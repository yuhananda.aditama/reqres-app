package id.bootcamp.reqres.di

import android.content.Context
import id.bootcamp.reqres.data.ExampleRepository
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.data.local.room.ExampleDatabase
import id.bootcamp.reqres.data.remote.retrofit.ApiConfig

object Injection {
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        return ExampleRepository.getInstance(exampleApiService,exampleDao)
    }

    fun provideReqresRepository(context: Context): ReqresRepository{
        val reqresApiService  = ApiConfig.getRegresApiService()
        return ReqresRepository.getInstance(reqresApiService)
    }
}