package id.bootcamp.reqres.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentLoginBinding
import id.bootcamp.reqres.ui.viewmodel.LoginViewModel
import id.bootcamp.reqres.ui.viewmodel.ReqresViewModelFactory

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, ReqresViewModelFactory.getInstance(requireContext()))
            .get(LoginViewModel::class.java)

        binding.btnLogin.setOnClickListener {
            binding.pgLoading.visibility = View.VISIBLE
            binding.btnLogin.visibility = View.GONE

            //Validasi
            val isFormValid: Boolean = isFormValid()

            if (isFormValid) {
                val email = binding.etEmail.text.toString().trim()
                val password = binding.etPassword.text.toString().trim()
                viewModel.login(email, password)
            } else{
                binding.pgLoading.visibility = View.GONE
                binding.btnLogin.visibility = View.VISIBLE
            }
        }

        viewModel.loginLiveData.observe(viewLifecycleOwner) {
            if (it != null) {
                if (it.isLoginSuccess == true) {
                    binding.btnLogin.findNavController()
                        .navigate(R.id.action_loginFragment_to_listUserFragment)
                } else {
                    Toast.makeText(
                        requireContext(), it.loginMessage,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                //Dibuat Null lg datanya, agar tidak ketrigger berkali2
                viewModel.loginLiveData.postValue(null)
                binding.pgLoading.visibility = View.GONE
                binding.btnLogin.visibility = View.VISIBLE
            }
        }

        binding.btnRegister.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

    }

    private fun isFormValid(): Boolean {
        val strEmail = binding.etEmail.text.toString().trim()
        val strPassword = binding.etPassword.text.toString().trim()

        //Variabel bantu untuk menyimpan apakah form valid atau tidak
        var isFormValid = true

        //Validasi Email
        if (strEmail.isEmpty()) {
            binding.etEmail.error = "Email tidak boleh kosong"
            isFormValid = false
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
            binding.etEmail.error = "Email tidak valid"
            isFormValid = false
        }
        //Validasi Password
        if (strPassword.isEmpty()) {
            binding.etPassword.error = "Password tidak boleh kosong"
            isFormValid = false
        }
        return isFormValid
    }
}