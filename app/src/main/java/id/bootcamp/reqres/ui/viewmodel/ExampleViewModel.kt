package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ExampleRepository
import id.bootcamp.reqres.data.ExampleViewData
import kotlinx.coroutines.launch

class ExampleViewModel(private val exampleRepository: ExampleRepository) : ViewModel() {
    val exampleData : MutableLiveData<ExampleViewData> = MutableLiveData()
    fun getExampleData() = viewModelScope.launch {
        exampleData.postValue(exampleRepository.getExample())
    }
}