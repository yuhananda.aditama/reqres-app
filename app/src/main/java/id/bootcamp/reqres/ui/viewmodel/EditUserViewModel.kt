package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.ui.data.User
import kotlinx.coroutines.launch

class EditUserViewModel(val reqresRepository: ReqresRepository) : ViewModel() {

    val editUserData = MutableLiveData<User>()

    fun editData(id: Int, name: String, job: String) = viewModelScope.launch {
        val user = reqresRepository.editUser(id, name, job)
        editUserData.postValue(user)
    }
}