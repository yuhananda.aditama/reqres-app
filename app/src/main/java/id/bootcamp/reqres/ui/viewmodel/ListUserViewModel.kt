package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.ui.data.User
import kotlinx.coroutines.launch

class ListUserViewModel(val reqresRepository: ReqresRepository) : ViewModel() {

    //Deklarasi / membuat Variabel
    val listUserLiveData = MutableLiveData<List<User>>()
    val isDeleteSuccess = MutableLiveData<Boolean>()


    //viewModelScope.launch -> menjalan code di beda thread
    fun getListUser() = viewModelScope.launch {
        val dataList = reqresRepository.getUserList()
        //Memanggil fungsi dari dalam variabel listUserLiveData
        //postValue -> fungsin untuk update data
        listUserLiveData.postValue(dataList)
    }

    fun deleteUser(id: Int) = viewModelScope.launch {
        val isSuccess = reqresRepository.deleteUser(id)
        isDeleteSuccess.postValue(isSuccess)
    }
}