package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.ui.data.LoginData
import kotlinx.coroutines.launch

class LoginViewModel(val reqresRepository: ReqresRepository) : ViewModel() {

    val loginLiveData: MutableLiveData<LoginData> = MutableLiveData()

    fun login(email: String, password: String) = viewModelScope.launch {
        //Proses kirim email & password ke repository
        val loginData = reqresRepository.login(email, password)
        loginLiveData.postValue(
            loginData
        )
    }
}