package id.bootcamp.reqres.ui.data

data class RegisterData(
    val isSuccess: Boolean,
    val registerMessage : String
)