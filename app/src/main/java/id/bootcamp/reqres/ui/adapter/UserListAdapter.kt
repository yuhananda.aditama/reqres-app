package id.bootcamp.reqres.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import id.bootcamp.reqres.R
import id.bootcamp.reqres.ui.data.User

class UserListAdapter(val listUser: List<User>) :
    RecyclerView.Adapter<UserListAdapter.ViewHolder>() {
    //2. buat variabel/object berdasarkan interface interfacenya
    private var onUserItemClickListener: OnUserItemClickListener? = null

    //1. Buat interfacenya
    interface OnUserItemClickListener {
        //parameter user digunakan untuk nanti menandai user mana yang di click
        fun onItemClick(user: User)
        fun onEditClick(user: User)
        fun onDeleteClick(user: User)
    }

    //3. Buat fungsi untuk koneksi dengan interface diluar kelas
    fun setOnUserItemClickCallback(onUserItemClickListener: OnUserItemClickListener) {
        this.onUserItemClickListener = onUserItemClickListener
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar = itemView.findViewById<CircleImageView>(R.id.ivAvatar)
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
        val btnEdit = itemView.findViewById<ImageButton>(R.id.btnEdit)
        val btnDelete = itemView.findViewById<ImageButton>(R.id.btnDelete)
    }

    //Proses ketika viewholder dibikin
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    //Dipanggil ketika viewnya terlihat di layar
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = listUser[position]
        Glide.with(holder.itemView).load(data.avatar).into(holder.ivAvatar)
        holder.tvName.text = "${data.firstName} ${data.lastName}"

        //Trigger aksi ketika function dalam interface dipanggil
        holder.itemView.setOnClickListener {
            onUserItemClickListener?.onItemClick(data)
        }

        holder.btnEdit.setOnClickListener {
            onUserItemClickListener?.onEditClick(data)
        }

        holder.btnDelete.setOnClickListener {
            onUserItemClickListener?.onDeleteClick(data)
        }

    }

    //Kita memberi tahu adapter, banyaknya datanya berapa?
    override fun getItemCount(): Int {
        return listUser.size
    }
}