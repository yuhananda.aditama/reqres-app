package id.bootcamp.reqres.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.reqres.data.local.entity.ExampleEntity
import java.util.concurrent.Executors

@Database(entities = [ExampleEntity::class], version = 1, exportSchema = false)
abstract class ExampleDatabase : RoomDatabase() {

    abstract fun exampleDao(): ExampleDao

    companion object {
        @Volatile
        private var instance: ExampleDatabase? = null
        fun getInstance(context: Context): ExampleDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    ExampleDatabase::class.java, "Example.db"
                ).addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        val executor = Executors.newSingleThreadExecutor()
                        executor.execute {
                            getInstance(context).exampleDao().insertPopulateData(PREPOPULATE_DATA)
                        }
                    }
                }).build()
            }

        val PREPOPULATE_DATA = listOf(
            ExampleEntity(1, "Title 1", "Desc 1"),
            ExampleEntity(2, "Title 2", "Desc 2")
        )
    }
}