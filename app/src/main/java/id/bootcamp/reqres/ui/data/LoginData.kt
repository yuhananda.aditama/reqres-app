package id.bootcamp.reqres.ui.data

data class LoginData(
    val isLoginSuccess : Boolean,
    val loginMessage : String
)
