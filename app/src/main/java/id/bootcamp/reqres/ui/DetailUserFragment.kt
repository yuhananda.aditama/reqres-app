package id.bootcamp.reqres.ui

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentDetailUserBinding
import id.bootcamp.reqres.ui.data.User

class DetailUserFragment : Fragment() {
    lateinit var binding : FragmentDetailUserBinding

    companion object {
        const val EXTRA_USER = "EXTRA_USER"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentDetailUserBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Mengambil data kiriman dari page sebelumnya
        var user : User? = null
        if (arguments != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                user = arguments?.getParcelable(EXTRA_USER, User::class.java)
            } else {
                user = arguments?.getParcelable(EXTRA_USER)
            }
        }

        //Set Menu Toolbar
        val activity = activity as AppCompatActivity
        //Code set toolbar
        activity.setSupportActionBar(binding.toolbar)
        //Code untuk ubah title
        activity.supportActionBar?.title = user?.firstName
        //Code untuk menambah back button di toolbar
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //Code untuk aksi ketika back button di click
        binding.toolbar.setNavigationOnClickListener {
            //code untuk tutup halaman ini
            it.findNavController().popBackStack()
        }

        //set gambar ke UI layout
        Glide.with(requireActivity()).load(user?.avatar).into(binding.civAvatar)
        //set nama di UI layout
        binding.tvName.text = user?.firstName + " " + user?.lastName
        //set email di UI layout
        binding.tvEmail.text = user?.email
    }
}