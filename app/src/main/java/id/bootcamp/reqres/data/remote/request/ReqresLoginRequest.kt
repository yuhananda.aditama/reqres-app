package id.bootcamp.reqres.data.remote.request

import com.google.gson.annotations.SerializedName

data class ReqresLoginRequest(

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
