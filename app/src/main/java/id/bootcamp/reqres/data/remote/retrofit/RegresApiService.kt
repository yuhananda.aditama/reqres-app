package id.bootcamp.reqres.data.remote.retrofit

import id.bootcamp.reqres.data.remote.request.ReqresCreateRequest
import id.bootcamp.reqres.data.remote.request.ReqresEditRequest
import id.bootcamp.reqres.data.remote.request.ReqresLoginRequest
import id.bootcamp.reqres.data.remote.request.ReqresRegisterRequest
import id.bootcamp.reqres.data.remote.response.ReqresCreateResponse
import id.bootcamp.reqres.data.remote.response.ReqresEditResponse
import id.bootcamp.reqres.data.remote.response.ReqresListUserResponse
import id.bootcamp.reqres.data.remote.response.ReqresLoginResponse
import id.bootcamp.reqres.data.remote.response.ReqresRegisterResponse
import id.bootcamp.reqres.data.remote.response.ReqresSingleUserResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface RegresApiService {

    @POST("login")
    suspend fun login(@Body reqresLoginRequest: ReqresLoginRequest)
            : ReqresLoginResponse

    @POST("register")
    suspend fun register(@Body reqresRegisterRequest: ReqresRegisterRequest)
            : ReqresRegisterResponse

    @GET("users")
    suspend fun getUser(@Query("page") page: Int): ReqresListUserResponse

    @GET("users/{id}")
    suspend fun getUserById(@Path("id") id: Int): ReqresSingleUserResponse

    @POST("users")
    suspend fun insertUser(@Body reqresCreateRequest: ReqresCreateRequest): ReqresCreateResponse

    @PUT("users/{id}")
    suspend fun editUser(
        @Body reqresCreateRequest: ReqresEditRequest,
        @Path("id") id: Int
    ): ReqresEditResponse

    @DELETE("users/{id}")
    suspend fun deleteUserById(@Path("id") id: Int): Response<String?>
}
