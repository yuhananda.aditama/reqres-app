package id.bootcamp.reqres.data.remote.response

import com.google.gson.annotations.SerializedName

data class ReqresLoginResponse(

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("error")
	val error: String? = null
)
