package id.bootcamp.reqres.ui

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentEditUserBinding
import id.bootcamp.reqres.ui.data.User
import id.bootcamp.reqres.ui.viewmodel.EditUserViewModel
import id.bootcamp.reqres.ui.viewmodel.ReqresViewModelFactory

class EditUserFragment : Fragment() {
    private lateinit var binding: FragmentEditUserBinding
    private lateinit var viewModel: EditUserViewModel
    var user: User? = null

    companion object {
        const val EXTRA_USER = "EXTRA_USER"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEditUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
            ViewModelProvider(this, ReqresViewModelFactory.getInstance(requireContext())).get(
                EditUserViewModel::class.java
            )

        if (arguments != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                user = arguments?.getParcelable(EXTRA_USER, User::class.java)
            } else {
                user = arguments?.getParcelable(EXTRA_USER)
            }
        }

        binding.etName.setText("${user?.firstName} ${user?.lastName}")

        binding.btnEditUser.setOnClickListener {
            val isFormValid: Boolean = isFormValid()

            if (isFormValid) {
                val txtName = binding.etName.text.toString().trim()
                val txtJob = binding.etJob.text.toString().trim()
                val id = user?.id ?: -1
                viewModel.editData(id, txtName, txtJob)
//                Toast.makeText(requireContext(), "Edit User Success!", Toast.LENGTH_SHORT).show()
//                binding.btnEditUser.findNavController().popBackStack()
            }
        }

        viewModel.editUserData.observe(viewLifecycleOwner) {
            if (it != null) {
                if (it.firstName.isEmpty()) {
                    Toast.makeText(
                        requireContext(),
                        "Edit User Gagal", Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Edit User Berhasil", Toast.LENGTH_SHORT
                    ).show()
                    binding.btnEditUser.findNavController().popBackStack()
                }
                viewModel.editUserData.postValue(null)
            }
        }
    }

    private fun isFormValid(): Boolean {
        val txtName = binding.etName.text.trim()
        val txtJob = binding.etJob.text.trim()

        var isFormValid = true

        if (txtName.isEmpty()) {
            isFormValid = false
            binding.etName.error = "Nama tidak boleh kosong"
        }

        if (txtJob.isEmpty()) {
            isFormValid = false
            binding.etJob.error = "Job tidak boleh kosong"
        }

        return isFormValid
    }
}