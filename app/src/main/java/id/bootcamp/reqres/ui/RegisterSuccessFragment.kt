package id.bootcamp.reqres.ui

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentRegisterSuccessBinding

class RegisterSuccessFragment : DialogFragment() {
    private lateinit var binding: FragmentRegisterSuccessBinding
    var onLoginButtonListener: OnLoginButtonListener? = null

    interface OnLoginButtonListener {
        fun onLoginButtonClick()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentRegisterSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLogin.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val fragment = parentFragment
        if (fragment is RegisterFragment) {
            //Pasang listener/ Setup Listener
            this.onLoginButtonListener = fragment.onLoginButtonListener
        }
    }

    override fun onDetach() {
        super.onDetach()
        //Bersih-bersih
        onLoginButtonListener = null
    }

    override fun onDismiss(dialog: DialogInterface) {
        onLoginButtonListener?.onLoginButtonClick()
        super.onDismiss(dialog)
    }
}