package id.bootcamp.reqres.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentAddUserBinding
import id.bootcamp.reqres.ui.viewmodel.AddUserViewModel
import id.bootcamp.reqres.ui.viewmodel.ReqresViewModelFactory

class AddUserFragment : Fragment() {
    private lateinit var binding: FragmentAddUserBinding
    private lateinit var viewModel: AddUserViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentAddUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
            ViewModelProvider(this, ReqresViewModelFactory.getInstance(requireContext())).get(
                AddUserViewModel::class.java
            )

        binding.btnTambahUser.setOnClickListener {
            val isFormValid: Boolean = isFormValid()

            if (isFormValid) {
                val txtName = binding.etName.text.toString().trim()
                val txtJob = binding.etJob.text.toString().trim()
                viewModel.addUser(txtName, txtJob)
//                Toast.makeText(requireContext(), "Add User Success!", Toast.LENGTH_SHORT).show()
//                binding.btnTambahUser.findNavController().popBackStack()
            }

        }

        viewModel.createdUser.observe(viewLifecycleOwner) { user ->
            if (user != null) {
                if (user.firstName.isEmpty()) {
                    Toast.makeText(
                        requireContext(),
                        "Add User Gagal", Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Add User Berhasil", Toast.LENGTH_SHORT
                    ).show()
                    binding.btnTambahUser.findNavController().popBackStack()
                }
                viewModel.createdUser.postValue(null)
            }
        }
    }

    private fun isFormValid(): Boolean {
        val txtName = binding.etName.text.trim()
        val txtJob = binding.etJob.text.trim()

        var isFormValid = true

        if (txtName.isEmpty()) {
            isFormValid = false
            binding.etName.error = "Nama tidak boleh kosong"
        }

        if (txtJob.isEmpty()) {
            isFormValid = false
            binding.etJob.error = "Job tidak boleh kosong"
        }

        return isFormValid
    }
}