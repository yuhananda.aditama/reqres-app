package id.bootcamp.reqres.data.remote.response

import com.google.gson.annotations.SerializedName

data class ReqresRegisterResponse(

	@field:SerializedName("id")
	val id:Int? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("error")
	val error: String? = null
)
