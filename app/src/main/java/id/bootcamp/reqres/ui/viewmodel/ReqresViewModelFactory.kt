package id.bootcamp.reqres.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.di.Injection

class ReqresViewModelFactory(private val reqresRepository: ReqresRepository) :
    ViewModelProvider.NewInstanceFactory() {
    //class ini dibuat per feature
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(reqresRepository) as T
        } else if (modelClass.isAssignableFrom(RegisterViewModel::class.java)) {
            return RegisterViewModel(reqresRepository) as T
        } else if (modelClass.isAssignableFrom(ListUserViewModel::class.java)) {
            return ListUserViewModel(reqresRepository) as T
        } else if (modelClass.isAssignableFrom(AddUserViewModel::class.java)) {
            return AddUserViewModel(reqresRepository) as T
        } else if (modelClass.isAssignableFrom(EditUserViewModel::class.java)) {
            return EditUserViewModel(reqresRepository) as T
        }
        return super.create(modelClass)
    }

    companion object {
        @Volatile
        private var instance: ReqresViewModelFactory? = null
        fun getInstance(context: Context): ReqresViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ReqresViewModelFactory(Injection.provideReqresRepository(context))
            }.also { instance = it }
    }
}