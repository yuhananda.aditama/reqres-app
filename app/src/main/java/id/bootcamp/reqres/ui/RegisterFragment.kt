package id.bootcamp.reqres.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import id.bootcamp.reqres.R
import id.bootcamp.reqres.databinding.FragmentRegisterBinding
import id.bootcamp.reqres.ui.viewmodel.RegisterViewModel
import id.bootcamp.reqres.ui.viewmodel.ReqresViewModelFactory

class RegisterFragment : Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    var onLoginButtonListener: RegisterSuccessFragment.OnLoginButtonListener =
        object : RegisterSuccessFragment.OnLoginButtonListener {
            override fun onLoginButtonClick() {
                binding.btnLogin.findNavController().popBackStack()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
            ViewModelProvider(this, ReqresViewModelFactory.getInstance(requireContext())).get(
                RegisterViewModel::class.java
            )

        binding.btnLogin.setOnClickListener {
            it.findNavController().popBackStack()
        }

        binding.btnRegister.setOnClickListener {
            //Validasi
            val isFormValid: Boolean = isFormValid()

            if (isFormValid) {
                //Saat Register Success
                val email = binding.etEmail.text.toString().trim()
                val password = binding.etPassword.text.toString().trim()
                viewModel.register(email, password)
            }
        }

        viewModel.registerLiveData.observe(viewLifecycleOwner) {
            if (it != null) {
                if (it.isSuccess) {
                    val registerSuccessFragment = RegisterSuccessFragment()
                    val fragmentManager = childFragmentManager
                    registerSuccessFragment.show(
                        fragmentManager, RegisterSuccessFragment::class.java.simpleName
                    )
                    viewModel.registerLiveData.postValue(null)
                } else {
                    Toast.makeText(
                        requireContext(),
                        it.registerMessage, Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun isFormValid(): Boolean {
        val strEmail = binding.etEmail.text.toString().trim()
        val strPassword = binding.etPassword.text.toString().trim()

        //Variabel bantu untuk menyimpan apakah form valid atau tidak
        var isFormValid = true

        //Validasi Email
        if (strEmail.isEmpty()) {
            binding.etEmail.error = "Email tidak boleh kosong"
            isFormValid = false
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
            binding.etEmail.error = "Email tidak valid"
            isFormValid = false
        }
        //Validasi Password
        if (strPassword.isEmpty()) {
            binding.etPassword.error = "Password tidak boleh kosong"
            isFormValid = false
        } else if (strPassword.length < 8 || strPassword.length > 12) {
            binding.etPassword.error = "Panjang Password harus antara 8 s.d 12"
            isFormValid = false
        }
        return isFormValid
    }
}