package id.bootcamp.reqres.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.reqres.data.ReqresRepository
import id.bootcamp.reqres.ui.data.User
import kotlinx.coroutines.launch

class AddUserViewModel(val reqresRepository: ReqresRepository) : ViewModel() {

    val createdUser = MutableLiveData<User>()

    fun addUser(name: String, job: String) = viewModelScope.launch {
        val user = reqresRepository.addUser(name, job)
        createdUser.postValue(user)
    }

}