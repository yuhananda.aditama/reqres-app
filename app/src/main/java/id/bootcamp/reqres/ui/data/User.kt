package id.bootcamp.reqres.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    val id: Int,
    val avatar: String,
    val firstName: String,
    val lastName: String,
    val email: String
) : Parcelable